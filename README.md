It's little Calendar app has been created as a demo of React+Redux skills.
Standard boilerplate is used for this stack. 

CRUD of 'reminders' is realised in the Calendar.
The most of screen sizes are supported (responsive designed).
For simplicity, some styles are implemented only on initial window size and not change on it's resize.
To look at such proper responsive styles if you resize window, please, reload the page.

As the app is created without back-end (it's using only for running project and there is no API and DB),
data is cached and stored in the Redux store only, so if reload the page data disappears.
Also, actions there are sync whereas in the reality we use Redux Thunk (or Saga) to handle async API requests.

To run project, in terminal:
1) Go to the project dir ('Calendar_v6')
2) Install packages - run command `npm i`
3) When packages have been loaded, run `npm start`
4) When project has been built, in terminal you will see the link to running project 
`http://localhost:3000`
5) Go to it in your browser (Chrome is privileged)
