import _ from 'lodash';
import {
  CONST_EDIT_ONE_REMINDER,
  CONST_ADD_REMINDER,
  CONST_EDIT_REMINDERS
} from '../../constants';

const add0BeforeIfNeeded = (value) => `${value < 10 ? 0 : ''}${value}`;

export const getDateInputFormat = (reminderDate) => {
  let year;
  let month;
  let day;
  const formattedValues = {};
  if (!_.isEmpty(reminderDate)) {
    const {
      year: reminderYear,
      month: reminderMonth,
      date: reminderDay
    } = reminderDate;
    year = reminderYear;
    month = reminderMonth + 1;
    day = reminderDay + 1;
  } else {
    const now = new Date();
    year = now.getFullYear();
    month = now.getMonth() + 1;
    day = now.getDate();
    const hours = now.getHours();
    const minutes = now.getMinutes();
    const formattedHours = add0BeforeIfNeeded(hours);
    const formattedMinutes = add0BeforeIfNeeded(minutes);
    formattedValues.time = `${formattedHours}:${formattedMinutes}`;
  }
  const formattedMonth = add0BeforeIfNeeded(month);
  const formattedDay = add0BeforeIfNeeded(day);
  formattedValues.date = `${year}-${formattedMonth}-${formattedDay}`;

  return formattedValues;
};

export const getDialogHeader = (contentType, formattedRemindersDate) => {
  switch (contentType) {
    case CONST_ADD_REMINDER:
      return 'Create Reminder';
    case CONST_EDIT_ONE_REMINDER:
      return 'Edit Reminder';
    case CONST_EDIT_REMINDERS:
      return formattedRemindersDate;
    default:
      return '';
  }
};
