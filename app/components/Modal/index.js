import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';
import ReactModal from 'react-responsive-modal';

import { reminderPropType } from '../../containers/propTypes';
import { getDateInputFormat, getDialogHeader } from './utils';
import { CONST_EDIT_ONE_REMINDER, CONST_ADD_REMINDER } from '../../constants';

export default class Modal extends PureComponent {
  static propTypes = {
    isOpen: PropTypes.bool,
    close: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    edit: PropTypes.func.isRequired,
    addReminderFn: PropTypes.func.isRequired,
    contentType: PropTypes.string,
    remindersToEdit: PropTypes.shape({
      year: PropTypes.number,
      month: PropTypes.number,
      date: PropTypes.number,
      reminders: PropTypes.arrayOf(reminderPropType)
    }),
    editReminderFn: PropTypes.func.isRequired,
    removeReminderFn: PropTypes.func.isRequired,
    reminderToEditId: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState = () => {
    const { date, time } = getDateInputFormat();

    return {
      text: '',
      date,
      time,
      color: '#000000'
    };
  };

  componentWillReceiveProps({
    remindersToEdit,
    reminderToEditId,
    contentType
  }) {
    const { date } = getDateInputFormat(remindersToEdit);
    if (reminderToEditId && contentType === CONST_EDIT_ONE_REMINDER) {
      const { reminders = [] } = remindersToEdit;
      this.setState({ ...reminders.find(({ id }) => id === reminderToEditId), date });
    } else {
      const stateToSet = this.getInitialState();
      if (date) {
        stateToSet.date = date;
      }
      this.setState(stateToSet);
    }
  }

  onInputChange = ({ target: { value, id } }) => this.setState({ [id]: value });

  onSave = () => {
    const { save, edit, contentType } = this.props;
    switch (contentType) {
      case CONST_ADD_REMINDER:
        return save(this.state);
      case CONST_EDIT_ONE_REMINDER:
        return edit(this.state);
      default:
        return null;
    }
  };

  inputs = [{ type: 'text', maxLength: 30 }, 'date', 'time', 'color'];

  FormInput = ({ type, ...props }) => [
    <label key={type} className="label" htmlFor={type}>{type}</label>,
    <input
      {...{
        ...props,
        type,
        id: type
      }}
      key={`${type}0`}
      value={this.state[type]}
      onChange={this.onInputChange}
    />
  ];

  close = () => {
    this.setState(this.getInitialState());
    this.props.close();
  };

  render() {
    const {
      isOpen = false,
      contentType,
      remindersToEdit: {
        reminders,
        ...remindersDate
      },
      editReminderFn,
      addReminderFn,
    } = this.props;

    const isAddingReminder = contentType === CONST_ADD_REMINDER;
    const isEditingOneReminder = contentType === CONST_EDIT_ONE_REMINDER;
    const { date: formattedRemindersDate } = getDateInputFormat(remindersDate);

    return (
      <ReactModal
        classNames={{ modal: 'modal' }}
        open={isOpen}
        onClose={this.close}
      >
        <div className="modal-header">{getDialogHeader(contentType, formattedRemindersDate)}</div>

        {isAddingReminder || isEditingOneReminder ?
          <div>
            {this.inputs.map((el) =>
              this.FormInput(typeof el === 'string' ? { type: el } : el))}
            <button className="primary-action-button" onClick={this.onSave}>Save</button>
          </div>
          :
          <div>
            <button className="add-reminder modal-button" onClick={addReminderFn()}>
              <i className="fas fa-plus" />
            </button>

            {reminders && reminders.length ?
              reminders.map(({
                id,
                time: reminderTime,
                text: reminderText,
                color: reminderColor
              }) => (
                <div className="reminder-to-edit" key={id}>
                  <div className="reminder" style={{ color: reminderColor }}>{reminderTime} - {reminderText}</div>
                  <button onClick={editReminderFn(id)}>
                    <i className="fas fa-edit" />
                  </button>
                  <button onClick={this.props.removeReminderFn(id)}>
                    <i className="fas fa-trash-alt" />
                  </button>
                </div>))
              :
              <div className="no-reminders-text">
                No reminders
              </div>
            }
          </div>
        }
      </ReactModal>
    );
  }
}
