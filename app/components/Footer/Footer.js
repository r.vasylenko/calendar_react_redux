import React from 'react';
import './style.scss';

const Footer = () => (
  <footer>
    <section></section>
    <section>Made with <span role="img" aria-label="heart-emoji">❤️</span> by <a href="https://apiko.com">Apiko</a></section>
  </footer>
);

export default Footer;
