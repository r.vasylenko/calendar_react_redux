import React from 'react';
import PropTypes from 'prop-types';
import { months, week } from '../constants';
import { storedRemindersPropType } from '../../propTypes';
import { getSortedReminders } from '../../HomePage/utils';
import { DayView } from './DayView';
import './style.scss';

export const Month = ({
  monthStructure,
  selectedMonth,
  reminders,
  selectedYear,
  editRemindersFn,
  switchMonthFn,
  goToday,
}) => (
  <div className="month">

    {/* month header **************** */}
    <div className="month-name">
      <button className="switcher" onClick={switchMonthFn(-1)}>
        <i className="fas fa-angle-left" />
      </button>

      <span className="text">{months[selectedMonth]}, {selectedYear}</span>

      <button className="switcher right" onClick={switchMonthFn(1)}>
        <i className="fas fa-angle-right" />
      </button>

      <button className="primary-action-button today-button" onClick={goToday}>Today</button>
    </div>
    {/* month header **************** */}

    {/* week day names */}
    <div className="week">
      {week.map((day) =>
        /* For narrow screens shows only 3 letters of week day names (Sun, Mon, etc.)
           Works only for initial loaded page size. To make it dynamically changing - need more complicate approach
           to make React class with state using on window resize event */
        <div key={day} className="day day-name">{window.innerWidth > 700 ? day : day.substr(0, 3)}</div>)}
    </div>

    {/* days */}
    <div className="month-cells">
      {monthStructure.map((monthWeek, i) => (
        /* eslint react/no-array-index-key: 0 */
        <div className="week week-place-holder" key={i}>
          {monthWeek.map(({ date, month }) => {
            /* it orders reminders by time - in life with back-end it should do it on the server */
            const sortedReminders = getSortedReminders({
              reminders,
              selectedYear,
              month,
              date
            });

            return (
              <DayView
                {...{
                  date,
                  month,
                }}
                key={`${date}${month}`}
                onEditClick={editRemindersFn(selectedYear, month, date, sortedReminders)}
                reminders={sortedReminders}
              />);
          })}
        </div>))}
    </div>
  </div>);

Month.propTypes = {
  monthStructure: PropTypes.arrayOf(PropTypes.array),
  selectedYear: PropTypes.number,
  selectedMonth: PropTypes.number,
  reminders: storedRemindersPropType,
  editRemindersFn: PropTypes.func.isRequired,
  switchMonthFn: PropTypes.func.isRequired,
  goToday: PropTypes.func.isRequired,
};
