import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { months } from '../constants';
import { getIsToday } from '../../HomePage/utils';
import { reminderPropType } from '../../propTypes';

export class DayView extends Component {
  static propTypes = {
    date: PropTypes.number,
    month: PropTypes.number,
    onEditClick: PropTypes.func.isRequired,
    reminders: PropTypes.arrayOf(reminderPropType)
  };

  state = {};

  componentDidMount() {
    this.showOverflowButtonIfNeeded(this.dayView, this.remindersContainer);
  }

  shouldComponentUpdate(nextProps, { overflowButtonStyle }) {
    const propsToCompare = ['date', 'month', 'reminders'];

    return !_.isEqual(_.pick(nextProps, propsToCompare), _.pick(this.props, propsToCompare)) ||
      !_.isEqual(overflowButtonStyle, this.state.overflowButtonStyle);
  }

  componentDidUpdate() {
    this.showOverflowButtonIfNeeded(this.dayView, this.remindersContainer);
  }

  /* It's positioning button 'Show all' depending on day cell content height (overflows or not).
        Works only on initial page height - to make it dynamically resizable it's needed more complex approach
        to save in state current window height and using window on resize method */
  showOverflowButtonIfNeeded = ({ offsetHeight }, { offsetHeight: remindersHeight }) => this.setState({
    overflowButtonStyle: {
      display: offsetHeight < remindersHeight + 22 ? 'inherit' : 'none',
      marginTop: offsetHeight - 28,
    }
  });

  render() {
    const {
      reminders,
      date,
      month,
      onEditClick,
    } = this.props;

    return (
      <div
        className={`day day-place-holder ${getIsToday({ date, month }) ? 'today' : ''}`}
        ref={(el) => { this.dayView = el; }}
        onClick={onEditClick}
        role="none"
      >
        <div>
          <span className="day-number">
            {date + 1}{date === 0 && ` ${months[month].substring(0, 3)}`}
          </span>
          <div className="edit-day-reminders"><i className="fas fa-edit" /></div>
        </div>

        <div className="reminders" ref={(el) => { this.remindersContainer = el; }}>
          {reminders && reminders.map(({
            id,
            time,
            text,
            color
          }) => <div key={id} style={{ color }}>{time} - {text}</div>)}
        </div>

        <div style={this.state.overflowButtonStyle} className="overflow-button">
          Show all {reminders && reminders.length}
        </div>
      </div>);
  }
}
