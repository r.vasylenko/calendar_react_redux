import PropTypes from 'prop-types';

export const reminderPropType = PropTypes.shape({
  color: PropTypes.string,
  date: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  text: PropTypes.string,
  time: PropTypes.string.isRequired,
});

export const storedRemindersPropType = PropTypes.objectOf(
  // year
  PropTypes.objectOf(
    // month
    PropTypes.objectOf(
      // date
      PropTypes.arrayOf(reminderPropType))
  )
);
