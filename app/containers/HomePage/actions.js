/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export const yourAction(payload) {
 *        return { type: YOUR_ACTION_CONSTANT, payload: var }
 *    }
 */

import {
  ADD_REMINDER,
  UPDATE_REMINDER,
  REMOVE_REMINDER
} from './actionNames';

/**
 * Changes the input field of the form
 *
 * @param  {name} name The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAME
 */

/* to make async actions to use back-end APIs, we use Redux Thunk's or Saga's */

export const addReminder = (payload) => ({
  type: ADD_REMINDER,
  payload
});

export const updateReminder = (payload) => ({
  type: UPDATE_REMINDER,
  payload
});

export const removeReminder = (payload) => ({
  type: REMOVE_REMINDER,
  payload
});
