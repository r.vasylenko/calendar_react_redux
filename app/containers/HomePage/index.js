import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import HomePage from './HomePage';
import { addReminder, updateReminder, removeReminder } from './actions';

const mapStateToProps = ({ home: { reminders } }) => ({ reminders });

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({
    // with back-end getReminders is needed also
    addReminder,
    updateReminder,
    removeReminder
  }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
