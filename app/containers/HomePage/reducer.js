/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 */

import _ from 'lodash';

import {
  ADD_REMINDER,
  UPDATE_REMINDER,
  REMOVE_REMINDER
} from './actionNames';

// The initial state of the App
/* replace to get some initial reminders in the calendar

(in reality it's stored in the DB on the back-end)
const initialState = {
  reminders: {
    2018: {
      12: {
        20: [
          {
            id: 1,
            text: 'Pass test bla-bla-bla-bla-bla',
            time: '19:00',
            color: '#1a2b3c'
          },
          {
            id: 2,
            text: 'Pass test',
            time: '20:00',
            color: '#4d5'
          },
        ]
      }
    }
  }
};
*/
const initialState = {
  reminders: {}
};

export default (state = initialState, { type, payload = {} }) => {
  const { reminders } = state;
  const { reminder = {}, remindersToEdit, updateRemindersView } = payload;
  const { date: fullDate = '' } = reminder;
  const [year, month, date] = (fullDate.split('-'));
  const yearPrevReminders = reminders[year] || {};
  const monthPrevReminders = yearPrevReminders[month - 1] || {};
  const dayReminders = monthPrevReminders[date - 1] || [];

  switch (type) {
    case ADD_REMINDER:

      return {
        ...state,
        reminders: {
          ...reminders,
          [year]: {
            ...yearPrevReminders,
            [month - 1]: {
              ...monthPrevReminders,
              [date - 1]: [
                ...dayReminders,
                { ...reminder }
              ]
            },
          }
        },
      };

    case UPDATE_REMINDER: {
      const remindersList = _.cloneDeep(dayReminders);
      const reminderToEditIndex = remindersList.findIndex(({ id }) => id === reminder.id);
      const isDateSame = reminderToEditIndex >= 0;
      if (isDateSame) {
        remindersList[reminderToEditIndex] = reminder;
        updateRemindersView({ ...remindersToEdit, reminders: remindersList });
      } else {
        remindersList.push(reminder);
      }

      return {
        ...state,
        reminders: {
          ...reminders,
          [year]: {
            ...yearPrevReminders,
            [month - 1]: {
              ...monthPrevReminders,
              [date - 1]: remindersList
            }
          },
        }
      };
    }

    case REMOVE_REMINDER: {
      const {
        year: remindersToEditYear,
        month: remindersToEditMonth,
        date: remindersToEditDate,
        reminders: remindersToEditList
      } = remindersToEdit;

      const yearPrevRemindersToEdit = reminders[remindersToEditYear];
      const monthPrevRemindersToEdit = yearPrevRemindersToEdit[remindersToEditMonth];
      const updatedReminders = _.cloneDeep(remindersToEditList);
      _.remove(updatedReminders, { id: reminder.id });
      if (updateRemindersView) {
        updateRemindersView({ ...remindersToEdit, reminders: updatedReminders });
      }

      return {
        ...state,
        reminders: {
          ...reminders,
          [remindersToEditYear]: {
            ...yearPrevRemindersToEdit,
            [remindersToEditMonth]: {
              ...monthPrevRemindersToEdit,
              [remindersToEditDate]: updatedReminders
            }
          }
        }
      };
    }

    default:
      return state;
  }
};
