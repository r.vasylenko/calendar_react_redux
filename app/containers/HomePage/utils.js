import { weeksListPlaceHolder } from '../Views/constants';

export const getMonthViewData = (shiftMonths) => {
  const now = new Date();
  const month = now.getMonth() + shiftMonths;
  const year = now.getFullYear();
  const selectedMonthDate = new Date(year, month);
  const selectedMonth = selectedMonthDate.getMonth();
  const selectedYear = selectedMonthDate.getFullYear();
  const firstDayOfMonth = selectedMonthDate.getDay();
  const lastDateOfMonth = new Date(year, month + 1, 0).getDate();
  const lastDateOfPrevMonth = new Date(year, month, 0).getDate();
  const prevMonthDate = new Date(year, month - 1);
  const prevMonth = prevMonthDate.getMonth();
  const prevMonthYear = prevMonthDate.getFullYear();
  const nextMonthDateObj = new Date(year, month + 1);
  const nextMonth = nextMonthDateObj.getMonth();
  const nextMonthYear = nextMonthDateObj.getFullYear();

  let monthDate = 0;
  let nextMonthDate = 0;
  const getMonthDateObjAndIncreaseDate = () => ({
    date: monthDate++,
    month: selectedMonth,
    year: selectedYear
  });

  const monthStructure = weeksListPlaceHolder.map((week, weekIndex) => week.map((day, dayIndex) => {
    if (weekIndex < 1) {
      if (dayIndex >= firstDayOfMonth) {
        return getMonthDateObjAndIncreaseDate();
      }

      return {
        date: (lastDateOfPrevMonth + dayIndex + 1) - firstDayOfMonth,
        month: prevMonth,
        year: prevMonthYear
      };
    } else if (weekIndex === 4) {
      if (monthDate < lastDateOfMonth) {
        return getMonthDateObjAndIncreaseDate();
      }

      return {
        date: nextMonthDate++,
        month: nextMonth,
        year: nextMonthYear
      };
    }
    return getMonthDateObjAndIncreaseDate();
  }));

  return {
    monthStructure,
    selectedMonth,
    selectedYear
  };
};

export const getSortedReminders = ({
  reminders,
  selectedYear,
  month,
  date
}) => {
  const yearReminders = reminders[selectedYear];
  const monthReminders = yearReminders && yearReminders[month];
  const dayReminders = monthReminders && monthReminders[date];

  return dayReminders && dayReminders.sort(({ time }, { time: nextTime }) =>
    time.replace(':', '') - nextTime.replace(':', ''));
};

export const getIsToday = ({ date, month }) => {
  const today = new Date();

  return today.getDate() === date + 1 && today.getMonth() === month;
};
