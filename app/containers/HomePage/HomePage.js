/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { Month } from '../Views/Month/';
import { MonthsList } from '../Views/MonthsList';
import Modal from '../../components/Modal';

import {
  CONST_ADD_REMINDER,
  CONST_EDIT_REMINDERS,
  CONST_EDIT_ONE_REMINDER,
} from '../../constants';
import { getMonthViewData } from './utils';
import { storedRemindersPropType } from '../propTypes';
import './style.scss';

export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    reminders: storedRemindersPropType,
    actions: PropTypes.objectOf(PropTypes.func)
  };

  state = {
    selectedView: 'month',
    selectedMonth: 0, // current,
    remindersToEdit: {},
    reminderToEditId: null
  };

  openModal = () => this.setState({ isModalOpen: true });

  closeModal = () => this.setState({ isModalOpen: false });

  switchMonthFn = (delta) => () => this.setState({ selectedMonth: this.state.selectedMonth + delta });

  saveReminder = (reminder) => {
    this.props.actions.addReminder({ reminder: { ...reminder, id: Date.now() } });
    this.closeModal();
  };

  updateReminder = (reminder) => {
    const { actions } = this.props;
    const { remindersToEdit, reminderToEditId } = this.state;
    actions.updateReminder({
      reminder,
      remindersToEdit,
      updateRemindersView: this.updateRemindersView
    });
    const { date: fullDate } = reminder;
    const [year, month, date] = (fullDate.split('-'));
    const { year: prevYear, month: prevMonth, date: prevDate } = remindersToEdit;
    // if reminder is moved to another day it needs to be removed from the current day
    if (+year !== prevYear || month - 1 !== prevMonth || date - 1 !== prevDate) {
      this.removeReminderFn(reminderToEditId)();
    }
    this.setState({
      reminderToEditId: null,
      modalContentType: CONST_EDIT_REMINDERS
    });
  };

  removeReminderFn = (id) => () => {
    const { remindersToEdit } = this.state;
    this.props.actions.removeReminder({
      reminder: { id },
      remindersToEdit,
      updateRemindersView: this.updateRemindersView
    });
  };

  updateRemindersView = (newRemindersToEdit) => this.setState({ remindersToEdit: newRemindersToEdit });

  addReminderFn = (remindersToEdit) => () => {
    const stateToSet = { modalContentType: CONST_ADD_REMINDER };
    if (remindersToEdit) {
      stateToSet.remindersToEdit = remindersToEdit;
    }
    this.setState(stateToSet);
    this.openModal();
  };

  editRemindersFn = (
    year,
    month,
    date,
    reminders
  ) => () => {
    this.setState({
      modalContentType: CONST_EDIT_REMINDERS,
      remindersToEdit: {
        year,
        month,
        date,
        reminders
      }
    });
    this.openModal();
  };

  editOneReminderFn = (id) => () => {
    this.setState({
      modalContentType: CONST_EDIT_ONE_REMINDER,
      reminderToEditId: id
    });
  };

  goToday = () => {
    this.setState({ selectedMonth: 0 });
  };

  render() {
    const {
      state,
      props,
      addReminderFn,
      editRemindersFn,
      removeReminderFn,
      switchMonthFn,
      goToday
    } = this;
    const {
      selectedView,
      selectedMonth,
      isModalOpen,
      modalContentType,
      remindersToEdit,
      reminderToEditId
    } = state;
    const { reminders } = props;

    return (
      <article>
        <Helmet>
          <title>Calendar</title>
          <meta name="description" content="Reminder" />
        </Helmet>

        <div className="home-page">
          <section className="centered">
            <h2>Calendar</h2>
            <button className="add-reminder" onClick={this.addReminderFn({})}>
              <i className="fas fa-plus" />
            </button>
          </section>

          <section>
            {selectedView === 'month' ?
              <Month
                {...{
                  ...getMonthViewData(selectedMonth),
                  reminders,
                  editRemindersFn,
                  switchMonthFn,
                  goToday
                }}
              />
              :
              <MonthsList />}{/* todo: year view */}
          </section>

          <Modal
            {...{
              reminderToEditId,
              remindersToEdit,
              removeReminderFn,
              addReminderFn,
              isOpen: isModalOpen,
              close: this.closeModal,
              save: this.saveReminder,
              edit: this.updateReminder,
              contentType: modalContentType,
              editReminderFn: this.editOneReminderFn,
            }}
          />
        </div>
      </article>
    );
  }
}
