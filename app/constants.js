/* prefix CONST lets to distinguish constants and action names */
export const CONST_ADD_REMINDER = 'addReminder';
export const CONST_EDIT_REMINDERS = 'editReminders';
export const CONST_EDIT_ONE_REMINDER = 'editOneReminder';
