/**
 * Create the store with dynamic reducers
 */

import {
  createStore,
  applyMiddleware,
  compose,
  combineReducers
} from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import homeReducer from './containers/HomePage/reducer';

export default function configureStore(initialState = {}, history) {

  const appReducer = combineReducers({
    router: routerReducer,
    home: homeReducer
  });

  const rootReducer = (state, action) => appReducer(state, action);

  const middleware = [
    // applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history)),
  ];

  // /*global IS_DEV*/
  // IS_DEV && middleware.push(applyMiddleware(createLogger()));

  return createStore(
    rootReducer,
    initialState,
    compose(...middleware)
  );
}
